# README #

This repository is meant to track and demonstrate my progress in completing Exercism exercises for various languages. All solutions are my own.

You can more easily look over my solutions and progress [here](https://exercism.io/profiles/digiorno331).