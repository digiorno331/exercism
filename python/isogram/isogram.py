def is_isogram(string):
    is_isogram           = True
    search_string        = string.lower().replace('-','').replace(' ','')
    search_string_length = len(search_string)

    for i in range( 0, search_string_length - 1 ):
        parseable_search_string = search_string[:i] + search_string[ i+1: ] # Remove current search character
        search_character        = search_string[i]

        if search_character in parseable_search_string:
            is_isogram = False
            break

    return is_isogram
