var Bob = function() {
};

Bob.prototype.hey = function (message) {
    message = message.trim();

    var ends_with_questionmark = /\?$/;
    var no_lowercase           = /^[^a-z]*$/;
    var atleast_one_uppercase  = /[A-Z]/;

    var contains_question_mark         = message.match( ends_with_questionmark );
    var contains_no_lowercase          = message.match( no_lowercase           );
    var contains_atleast_one_uppercase = message.match( atleast_one_uppercase  );

    var question = false;
    var yelling  = false;

    if( contains_question_mark !== null )
        question = true;

    if( contains_no_lowercase && contains_atleast_one_uppercase )
        yelling = true;

    // Bob's default response is 'Whatever.'
    var response = 'Whatever.';

    if( yelling && question )
        response = 'Calm down, I know what I\'m doing!';
    else if( yelling )
        response = 'Whoa, chill out!';
    else if( question )
        response = 'Sure.';
    else if( message.length === 0 )
        response = 'Fine. Be that way!';

    return response;
}

module.exports = Bob;
