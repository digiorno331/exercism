var DnaTranscriber = function () {};

DnaTranscriber.prototype.toRna = function( dna_string ) {
    if( typeof dna_string !== 'string' )
    {
        throw new Error( 'Invalid input' );
    }

    var rna_string = '';

    for( var i=0; i < dna_string.length; i++ )
    {
        switch( dna_string[i] ) {
            case 'G':
                rna_string += 'C';
                break;
            case 'C':
                rna_string += 'G';
                break;
            case 'T':
                rna_string += 'A';
                break;
            case 'A':
                rna_string += 'U';
                break;
            default:
                throw new Error( 'Invalid input' );
                break;
        }
    }

    return rna_string;
}

module.exports = DnaTranscriber;
