var Gigasecond = function (start_date) {
    this.start_date = start_date;
};

Gigasecond.prototype.date = function () {
    return new Date( this.start_date.getTime() + Math.pow( 10, 12 ) );
};

module.exports = Gigasecond;
