var Pangram = function( pangram_string )
{
    if( typeof pangram_string !== 'string' )
        throw new Error( 'bad string' );

    this.pangram_string = pangram_string;
}

Pangram.prototype.isPangram = function()
{
    var is_pangram = true;
    var alphabet   = 'abcdefghijklmnopqrstuvwxyz';

    if( this.pangram_string.length === 0 )
    {
        is_pangram = false;
    }
    else
    {
        var lowercase_pangram_string = this.pangram_string.toLowerCase();

        // Loop over the alphabet and ensure that each letter ecists within the string.
        for( var i=0; i < alphabet.length; i++ )
        {
            if( lowercase_pangram_string.indexOf( alphabet[i] ) === -1 )
            {
                is_pangram = false;
                break;
            }
        }
    }

    return is_pangram;
}

module.exports = Pangram;
