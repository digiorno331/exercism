var reverseString = function ( string ) {

    if( typeof string !== 'string' )
        throw new Error( 'Bad String' );

    var string_array = string.split('');

    var lead_index      = null;
    var tail_index      = null;
    var lead_character  = null;
    var tail_character  = null;

    if( string_array.length > 0 )
    {
        // Loop over first half of the provided string
        for( var i=0; i < (string_array.length / 2); i++ )
        {
            lead_index = i;
            tail_index = string_array.length - i - 1;

            lead_character = string[ lead_index ];
            tail_character = string[ tail_index ];

            string_array[lead_index] = tail_character;
            string_array[tail_index] = lead_character;
        }
    }

    var reversed_string = string_array.join('');

    return reversed_string;
};

module.exports = reverseString;
