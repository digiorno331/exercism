var Cipher = function (key) {
    var characters = 'abcdefghijklmnopqrstuvwxyz';
    this.characters = characters;

    if( typeof key !== 'string' )
    {
        var key = '';

        // Create Random Key
        for( var i=0; i < 100; i++ )
        {
            key += characters.charAt( Math.floor( Math.random() * characters.length ) );
        }
    }
    else
    {
        if( key.length <= 0 )
            throw new Error( 'Bad key' );

        // Ensure the key provided is only comprised of lower-case letters
        for( var j=0; j < key.length; j++ )
        {
            if( characters.indexOf( key[j] ) === -1 )
                throw new Error( 'Bad key' );
        }
    }

    this.key = key;
};

Cipher.prototype.encode = function (message) {
    return this.encode_decode_helper( message, 'encode' );
}

Cipher.prototype.decode = function (message) {
    return this.encode_decode_helper( message, 'decode' );
}

Cipher.prototype.encode_decode_helper = function ( message, direction )
{
    if( typeof message !== 'string' )
    {
        throw new Error( 'Invalid message' );
    }

    var encoded_message   = '';
    var current_character = null;
    var key               = this.key;
    var characters        = this.characters;

    for( var i=0; i < message.length; i++ )
    {
        // "loop" over key
        key_index             = i % key.length;
        current_key_character = key.charAt( key_index );

        current_message_character = message.charAt( i );

        current_message_character_index = characters.indexOf( current_message_character );
        key_offset                      = characters.indexOf( current_key_character )

        if( direction === 'encode' )
            encoded_index = ( current_message_character_index + key_offset ) % characters.length;
        else
            encoded_index = ( current_message_character_index + ( characters.length - key_offset ) ) % characters.length;

        encoded_message += characters.charAt( encoded_index );
    }

    return encoded_message;
}

module.exports = Cipher;
