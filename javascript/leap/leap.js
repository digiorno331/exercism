//
// This is only a SKELETON file for the "Leap" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

var Year = function (input) {
    this.year = input;
};

Year.prototype.isLeap = function () {
    var year = this.year;
    var is_leap_year = false;

    if(      (year % 400) === 0 ) is_leap_year = true;
    else if( (year % 100) === 0 ) is_leap_year = false;
    else if( (year %   4) === 0 ) is_leap_year = true;

    return is_leap_year;
};

module.exports = Year;
